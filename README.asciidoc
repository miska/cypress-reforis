Cypress project to go through reForis Wizzard
=============================================

Goal
----

The aim was to make it easy to go through whole wizzard on newly flashed router
and verify that it works. And to get some screenshots on the way.

Running
-------

It depends on Cypress[1], so the first step is to install Cypress

-------------------------------------------------------------------------------
$ npm install -g cypress
-------------------------------------------------------------------------------

Next step is to either open GUI to edit the tests via

-------------------------------------------------------------------------------
$ cypress open --project .
-------------------------------------------------------------------------------

Or to run whole test suite using

-------------------------------------------------------------------------------
$ cypress run --headless
-------------------------------------------------------------------------------

Options
~~~~~~~

To make it easy to debug and develop tests, there are some assumptions. Either
you do factory reset before every run, or you keep `SETUP` set to `true` in
`cypress.json` and get your ssh key to the roots authorized keys on your
router. You can also modify the IP address of your router in `cypress.json`.

Tests
~~~~~

Currently there is only one test and that one is kinda special as it is for
first run wizard and thus the steps follow closely each other. You can find it in
file `cypress/integration/wizzard.js`.
