// reForis wizzard tests

describe('Test reForis wizzard', function() {
    it('Opens wizzard', function() {
        // Reset wizzard
        if(Cypress.env('SETUP')) {
            cy.exec('ssh root@' + Cypress.env('IP') + ' echo \\"\\" \\> /etc/config/foris')
        }
        cy.clearCookies()
        Cypress.Cookies.defaults({preserve: 'session'})

        // Redirects to password settings?
        cy.visit("/")
        cy.url().should('include', 'guide/password')
    })

    it('Seting password works', function() {
        // Type incorrect password
        cy.get('.mb-3 > :nth-child(1)').type('turris')
        cy.get('.mb-3 > :nth-child(2)').type('turris1')
        // Does error show?
        cy.get('.invalid-feedback').should('be.visible')
        cy.screenshot('wizzard_01.01_password_wrong.png')
        // Type correct password
        cy.log("Testing matching passwords")
        cy.get('.mb-3 > :nth-child(2)').clear()
        cy.get('.mb-3 > :nth-child(2)').type('turris')
        cy.get('.invalid-feedback').should('not.exist')
        cy.screenshot('wizzard_01.02_password_right.png')
        // Test hiding of Advanced Administration Password
        cy.get('.p-4').should('contain', 'Advanced Administration Password')
        cy.get('.custom-control').click()
        cy.get('.p-4').should('not.contain', 'Advanced Administration Password')
        cy.screenshot('wizzard_01.03_password_same.png')
        // Save changes
        cy.get('.text-right > .btn').click()
        cy.get('.card-body').should('contain', 'Your password is set')
        cy.get('.p-4 > h2').should('contain', 'Password Settings')
        cy.get('.btn-success').should('be.visible')
        cy.get('.btn-success').should('contain', 'Next step')
        cy.screenshot('wizzard_01.04_password_done.png')
        cy.get('.btn-success').click()
        cy.url().should('include', 'guide/profile')
    })

    it('Guide workflow sellection works', function() {
        cy.get('#workflow-selector').should('contain', 'Router')
        cy.get('#workflow-selector').should('contain', 'Minimal')
        cy.screenshot('wizzard_02.01_guide_selector_shows.png')
        cy.get(':nth-child(1) > .workflow > .btn > .img-fluid').click()
        cy.get('#workflow-selector').should('contain', 'Router')
        cy.screenshot('wizzard_02.01_guide_selector_done.png')
        cy.get('.btn-success').click()
        cy.url().should('include', 'guide/networks')
    })

    it('Networks are set right', function() {
        cy.get(':nth-child(3) > .scrollable').should('contain','WAN')
        cy.get(':nth-child(6) > .scrollable').should('contain','LAN1')
        cy.screenshot('wizzard_03.01_networks_init.png')
        cy.get('.text-right > .btn').click()
        cy.get('.spinner-text').should('be.visible')
        cy.get('.spinner-text').should('not.exist')
        cy.get(':nth-child(3) > .scrollable').should('contain','WAN')
        cy.get(':nth-child(6) > .scrollable').should('contain','LAN1')
        cy.screenshot('wizzard_03.01_networks_done.png')
        cy.get('.btn-success').click()
        cy.url().should('include', 'guide/wan')
    })

    it('WAN settings work', function() {
        if(Cypress.env('SETUP')) {
            cy.exec('ssh root@' + Cypress.env('IP') + ' "uci set network.wan.proto=none; uci commit network.wan; /etc/init.d/network restart"')
        }
        cy.get('.card.col-sm-12').should('contain', 'IPv4')
        cy.get('.card.col-sm-12').should('contain', 'IPv6')
        cy.screenshot('wizzard_04.01_wan.png')
        cy.get('.col-lg-12 > .btn').click()
        cy.get('tbody > :nth-child(1) > .text-right div').should('have.class', 'text-danger')
        cy.screenshot('wizzard_04.02_wan_broken.png')
        cy.get('.card.col-sm-12 > form > .text-right > .btn').click()
        cy.get('.spinner-text').should('be.visible')
        cy.get('.spinner-text').should('not.exist')
        cy.get('.col-lg-12 > .btn').click()
        cy.get('tbody > :nth-child(1) > .text-right div').should('not.have.class', 'text-danger')
        cy.screenshot('wizzard_04.02_wan_works.png')
        cy.get('.btn-success').click()
        cy.url().should('include', 'guide/time')
    })

    it('Setting time is possible', function() {
        cy.get('form').should('contain','Prague')
        cy.screenshot('wizzard_05.01_time.png')
        cy.get('.text-right > .btn').click()
        cy.get('form').should('contain','Prague')
        cy.screenshot('wizzard_05.02_time_saved.png')
        cy.get('.btn-success').click()
        cy.url().should('include', 'guide/dns')
    })

    it('CZ.NICs DNS works', function() {
        cy.get('form').should('contain','DNS Settings')
        cy.screenshot('wizzard_06.01_dns.png')
        cy.get(':nth-child(2) > :nth-child(1) > .custom-control').click()
        cy.get(':nth-child(7) > .btn').click()
        cy.get('.spinner-border').should('be.visible')
        cy.get('.spinner-border').should('not.exist')
        cy.get('.col-lg-12 > .btn').click()
        cy.get('tbody > :nth-child(1) > .text-right div').should('have.class', 'text-success')
        cy.screenshot('wizzard_06.02_dns_tested.png')
        cy.get(':nth-child(2) > :nth-child(1) > .custom-control').click()
        cy.get(':nth-child(7) > .btn').click()
        cy.get('.spinner-border').should('be.visible')
        cy.get('.spinner-border').should('not.exist')
        cy.get('.btn-success').click()
        cy.url().should('include', 'guide/updater')
    })

    it('Updater settings are there', function() {
        cy.get('.p-4').should('contain', 'Automatic Update Settings')
        cy.screenshot('wizzard_07.01_updater.png')
        cy.get('.text-right > .btn').click()
        cy.get('.p-4').should('contain', 'Automatic Update Settings')
        cy.screenshot('wizzard_07.02_updater_done.png')
        cy.get('.btn-success').click()
        cy.url().should('include', 'guide/finish')
    })

    it('Guide finished', function() {
        cy.get('h2').should('contain', 'Guide Finished')
        cy.screenshot('wizzard_08_finished.png')
        cy.get('.text-right > .btn').click()
        cy.url().should('not.include', 'guide')
    })

    it('Overview loaded', function() {
        cy.get('h1').should('contain', 'Overview')
        cy.screenshot('wizzard_09_overview.png')
    })
})
